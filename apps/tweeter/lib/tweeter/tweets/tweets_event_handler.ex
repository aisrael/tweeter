defmodule Tweeter.TweetsEventHandler do
  @moduledoc """
  Handles Tweet events
  """

  use GenServer
  require Logger

  alias Tweeter.Repo
  alias Tweeter.Tweet

  @spec start_link(term()) :: GenServer.on_start()
  def start_link(stream: stream) do
    Logger.debug("start_link(stream: #{stream})")
    GenServer.start_link(__MODULE__, [stream: stream], name: __MODULE__)
  end

  @impl true
  def init(stream: stream) do
    Logger.debug(fn -> "init(:ok)" end)
    res = Extreme.subscribe_to(Tweeter.EventStore, self(), stream)
    Logger.debug(fn -> "res => #{inspect(res)}" end)
    {:ok, :ok}
  end

  # Handle events from EventStore
  @impl true
  def handle_info({:on_event, push}, state) do
    Logger.debug(fn -> "New event: #{inspect(push)}" end)

    push.event.data
    |> :erlang.binary_to_term()
    |> process_event(push.event.event_type)

    {:noreply, state}
  end

  defp process_event(
         %{id: id, handle: handle, content: content, timestamp: timestamp} = data,
         "Tweeter.TweetCreated"
       ) do
    Logger.debug(fn -> "process_event(#{inspect(data)})" end)

    # TODO: Move all this logic inside `TweetCreated`
    {:ok, inserted_at} = DateTime.from_unix(timestamp, :millisecond)

    Logger.debug(fn -> "inserted_at => #{inspect(inserted_at)}" end)

    attrs = %{
      id: id,
      handle: handle,
      content: content,
      inserted_at: inserted_at |> DateTime.truncate(:second)
    }

    %Tweet{}
    |> Tweet.insert(attrs)
    |> Repo.insert()
  end
end
